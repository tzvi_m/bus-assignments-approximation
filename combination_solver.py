import networkx as nx
import os
from output_scorer import score_output  # added
import numpy as np
from networkx.algorithms.community import greedy_modularity_communities
from networkx.algorithms.community.quality import intra_community_edges
import copy
from random import shuffle


# THIS REQUIRES STUDENTS TO BE ONE AND IN ONLY ONE COMMUNITY. IF STUDENTS ARE IN MULTIPLE COMMUNITIES OR NO COMMUNITIES THAT NEEDS TO BE ADDRESSED.
# WHEN SHOULD I BE REMOVING THINGS FROM graph G?
# Should I remove a community from G and then recreate the communities list?
# TRACK SOME STATS?

def combination_solve(G, num_buses, size_bus, rowndy_groups, size, submission_num):
    input_path = "/Users/erinbeitel/projects/cs170/bus_assignments/all_inputs/{}/{}".format(size, submission_num)
    output_path = "/Users/erinbeitel/projects/cs170/bus_assignments/combosolver_outputs/{}/{}.out".format(size,
                                                                                                          submission_num)

    fieldTrip = FieldTrip(size, submission_num, G, rowndy_groups, num_buses, size_bus)
    fieldTrip.assignBuses()
    buses = fieldTrip.assignments()

    # ds for mapping rowdy groups to buses?

    file = open(output_path, "w")
    # print("BUSES: {}".format(buses))
    print("num buses, bus capacity: {}, {}".format(fieldTrip.num_buses, fieldTrip.size_bus))

    for b in buses:
        file.write("{}\n".format(b))

    if len(buses) > num_buses:
        print("Random solver used too many buses for {} input number {}".format(size, submission_num))
    file.close()
    score = score_output(input_path, output_path)  # score is returned as a tuple
    return score, output_path


class Bus:
    def __init__(self, capacity, number):
        self.children = []
        self.capacity = capacity
        self.number = number  # bus number
        self.community_numbers = []

    def children(self):
        return self.children

    def capacity(self):
        return self.capacity

    def community_numbers(self):
        return self.community_numbers

    def number(self):
        return self.number

    def count(self):
        return len(self.children)

    def availability(self):
        return self.capacity - self.count()

    def has_availability(self):
        return self.availability() > 0

    def add_student(self, student):
        if self.has_availability():
            self.children.append(student)
            return True
        else:
            return False

    def add_several_students(self, student_group):
        if len(student_group) <= self.availability():
            for s in student_group:
                self.children.append(s)
            return True
        else:
            return False

    def add_community_number(self, community_number):
        self.community_numbers.append(community_number)

    # REFACTOR NAMING OF THE FOLLOWING TWO:

    # returns how many nodes are affected by creating rowdy groups.
    def completes_rowdy_group(self, student, rowdy_groups):
        # potential_bus = copy.deepcopy(self.children)
        # potential_bus.append(student)
        present, groups = self.identifyRowdyGroups(student, rowdy_groups)
        rowdy_conflicts = [rowdy_groups[i] for i in groups]
        nodes_affected = 0
        for r in rowdy_conflicts:
            nodes_affected += len(r)
        return nodes_affected  # present, groups,

    def identifyRowdyGroups(self, student, rowdy_groups):
        groups = []
        potential_bus = copy.deepcopy(self.children)
        potential_bus.append(student)  # need to add the new student to the potential bus
        present = False
        i = 0  # rowdy group index
        for rowdy_group in rowdy_groups:
            rowdy = False
            for student in rowdy_group:
                if student not in potential_bus:
                    rowdy = False
                    break
                else:
                    rowdy = True
            if rowdy == True:
                groups.append(i)
            i += 1
        if groups:
            present = True
        return present, groups

    def rowdyGroupsFormedByAddingGroup(self, rowdy_groups, student_group):
        groups = []
        potential_bus = copy.deepcopy(self.children)
        for s in student_group:
            potential_bus.append(s)  # need to add the new student to the potential bus
        present = False
        i = 0  # rowdy group index
        for rowdy_group in rowdy_groups:
            rowdy = False
            for student in rowdy_group:
                if student not in potential_bus:
                    rowdy = False
                    break
                else:
                    rowdy = True
            if rowdy == True:
                groups.append(i)
            i += 1
        if groups:
            present = True
        return present, groups

    def nodesAffectedCreatingRowdysByAddingGroup(self, rowdy_groups, student_group):
        present, groups = self.rowdyGroupsFormedByAddingGroup(rowdy_groups, student_group)

        rowdy_conflicts = [rowdy_groups[i] for i in groups]
        nodes_affected = 0
        for r in rowdy_conflicts:
            nodes_affected += len(r)
        return nodes_affected


class FieldTrip:
    def __init__(self, size, number, graph, constraints, num_buses, size_bus):
        modularity_communities = greedy_modularity_communities(graph)
        # sort in order of number of inter community edges
        communities = sorted(modularity_communities, key=lambda x: intra_community_edges(graph, [x]))

        self.size = size
        self.number = number
        self.buses = [Bus(size_bus, i) for i in range(0, num_buses)]
        self.communities = [Community(students, i) for i, students in enumerate(communities, 1)]
        self.rowdy_groups = constraints  # the index in this array is the roudy group number.
        self.graph = graph
        self.large_communities = []
        self.size_bus = size_bus
        self.num_buses = num_buses

    def size(self):
        return self.size

    def number(self):
        return self.number

    def buses(self):
        return self.buses

    def communities(self):
        return self.communities

    def rowdy_groups(self):
        return self.rowdy_groups

    def graph(self):
        return self.graph

    def size_bus(self):
        return self.size_bus

    def num_buses(self):
        return self.num_buses

    def assignBuses(self):
        # for i in self.communities:
        #   print("community: {}".format(i.students))
        # for j in self.rowdy_groups:
        #   print("rowdy group: {}".format(j))
        self.assignCommunities()
        self.assignExtraStudents()
        if any(b == [] for b in [bus.children for bus in self.buses]):
            self.manageEmptyBuses()

    def assignCommunities(self):
        for community in self.communities:
            community.identifyRowdyGroups(self.rowdy_groups)
            community.breakRowdyGroups(self.rowdy_groups, self.graph)
            self.assignCommunity(community)  # assign kids by community
            # self.assignCommunityPieces(community) # try assigning all kids individually!
        for large_community in self.large_communities:
            self.assignCommunityPieces(large_community)  # break up communities

    # note none of teh community number stuff is being saved
    def manageEmptyBuses(self):
        options = []
        empty_bus_numbers = [i for i, bus in enumerate(self.buses, 0) if bus.children == []]
        # print("empty bus numbers: {}".format(empty_bus_numbers))

        while empty_bus_numbers:
            for bus in self.buses:
                if bus.children == [] or len(bus.children) == 1:
                    options.append(["foo", 1000])
                else:
                    values = [sum([1 for i in nx.all_neighbors(self.graph, student)]) for student in bus.children]
                    index = values.index(min(values))
                    kid = bus.children[index]
                    options.append([kid, min(values)])

            # print("options: {}".format(options))
            # print("min k[1]: {}".format(min(k[1] for k in options)))
            # bus_number = values.index(min(k[1] for k in options))
            min_value, bus_number = min((val[1], i) for i, val in enumerate(options))
            # bus_number = values.index(min(options, lambda x : x[1]))
            # print("min value : {}".format(min_value))

            # print("bus number : {}".format(bus_number))
            kid_to_remove = options[bus_number][0]
            # print("removing {}".format(kid_to_remove))
            # print("bus for removal: {}".format(self.buses[bus_number].children))
            self.buses[bus_number].children.remove(kid_to_remove)
            empty_bus_number = empty_bus_numbers.pop()
            self.buses[empty_bus_number].children.append(kid_to_remove)
            options = []

    def assignExtraStudents(self):
        for community in self.communities:
            self.assignExtras(community)

    # I think the following two could be moved to Community but a reference to self.buses would need to
    # be passed in such that it gets mutated in the Community method.
    def assignCommunity(self, community):
        student_group = community.students
        l = len(student_group)
        done = False
        # OPTIMIZATION:
        # could find best free bus at this point by looking at conflicts -
        # make an array of scores for the relative value of the connunity in each bus and then choose.
        for bus in self.buses:
            if bus.children != []:
                print("We are trying to assign students to a bus that isn't empty")
            done = bus.add_several_students(student_group)
            if done:
                community.add_bus_number(bus.number)
                community.asssigned()  # not sure what this is useful for...
                bus.add_community_number(community.number)
                break

        if done == False:
            # self.assignCommunityPieces(community) # break up communities
            self.large_communities.append(community)

    # I think the following two could be moved to Community but a reference to self.buses would need to
    # be passed in such that it gets mutated in the Community method.
    def assignCommunity(self, community):
        # print("number of buses, size of bus: {}, {}".format(self.num_buses, self.size_bus))

        student_group = community.students
        l = len(student_group)
        done = False
        bus_occupancy = [len(bus.children) for bus in self.buses]
        min_occupancy = min(bus_occupancy)
        empty_bus_index = bus_occupancy.index(min(bus_occupancy))

        has_bus_space = [1 if bus.availability() - l >= 0 else -1 for bus in
                         self.buses]  # 100 is used to represent a really bad bus (no space)
        nodes_affected = [bus.nodesAffectedCreatingRowdysByAddingGroup(self.rowdy_groups, community.students) + 1 for
                          bus in self.buses]
        hueristic = [a * b for a, b in zip(nodes_affected, has_bus_space)]
        if any(i >= 0 for i in hueristic) == False:
            self.assignCommunityPieces(community)
            return
        hueristic_index = hueristic.index(min(i for i in hueristic if i >= 0))

        if min_occupancy == 0:
            self.assignCommunityToBus(community, empty_bus_index)
        else:
            self.assignCommunityToBus(community, hueristic_index)

    def assignCommunityToBus(self, community, bus_number):
        bus = self.buses[bus_number]

        done = bus.add_several_students(community.students)
        if done:
            community.add_bus_number(bus.number)
            community.asssigned()  # not sure what this is useful for...
            bus.add_community_number(community.number)

        if done == False:
            print("oops the community didn't fit")
            # self.assignCommunityPieces(community) # break up communities
            self.large_communities.append(community)

    # When the community size is larger than the bus capacity, split up the community.
    def assignCommunityPieces(self, community):
        # OPTIMIZATION: Roudy groups might get split up.. If they dont need to keep track of which bus they went in. Rowdy group object??
        # OPTIMIZATION: Could figure out best place to put the kid community piece by scoring the possibilities.

        # TEMPORARYILY STUBBING THIS OUT:
        students = copy.deepcopy(community.students)
        for s in students:
            community.extra_students.append(s)
            community.students.remove(s)
        community.assigned = True

    def assignExtras(self, community):
        if community.extra_students == []: return

        num = community.number
        extra_students = copy.deepcopy(community.extra_students)
        l = len(extra_students)

        bus_occupancy = []
        min_occupancy = []
        empty_bus_index = []

        has_bus_space = []
        nodes_affected = []
        hueristic = []
        shuffle(extra_students)  # Add randomization so solver performs differently each time.
        for student in extra_students:
            bus_occupancy = [len(bus.children) for bus in self.buses]
            min_occupancy = min(bus_occupancy)
            empty_bus_index = bus_occupancy.index(min(bus_occupancy))

            has_bus_space = [1 if bus.availability() >= 1 else -1 for bus in self.buses]
            actual_bus_space = [bus.availability() for bus in self.buses]
            nodes_affected = [bus.completes_rowdy_group(student, self.rowdy_groups) for bus in self.buses]
            hueristic = [(a + 1) * b for a, b in zip(nodes_affected, has_bus_space)]

            if min(nodes_affected) > 0:
                print("** Rowdy conflict optinos: {} **".format(nodes_affected))
                print("** The best bus for {} has {} rowdy conflicts. **".format(student, min(nodes_affected)))

            if min_occupancy == 0:
                self.assignExtra(community, student, empty_bus_index)
            elif any(i >= 0 for i in
                     hueristic) == False:  # this is when we can't assign the heuristic because they are all negative.
                hueristic_index = hueristic.index(max(i for i in hueristic if i < 0))
                self.assignExtra(community, student, hueristic_index)
            else:
                hueristic_index = hueristic.index(min(
                    i for i in hueristic if i >= 0))  # index of bus with min rowdy conflicts, that actually has space.
                self.assignExtra(community, student, hueristic_index)

        if community.extra_students != []:
            print("not all students were assigned!!!")

    def assignExtra(self, community, student, bus_index):
        done = False
        done = self.buses[bus_index].add_student(student)
        if done:
            community.add_bus_number(self.buses[bus_index].number)
            self.buses[bus_index].add_community_number(community.number)
            community.extra_students.remove(student)
        else:
            print("student {} not assigned!!!!".format(student))
        # print("self.buses[bus_index].children: {}".format(self.buses[bus_index].children))

    def assignments(self):
        return [b.children for b in self.buses]


class Community:
    def __init__(self, students, number):
        self.students = list(students)
        self.bus_numbers = []  # Not sure if this is necessary, but its which bueses the community is in.
        self.rowdy_group_numbers = []
        self.extra_students = []
        self.assigned = False
        self.number = number  # number of community

    def students(self):
        return self.students

    def number(self):
        return self.number

    def bus_numbers(self):
        return self.bus_numbers

    def rowdy_group_numbers(self):
        return self.rowdy_group_numbers

    def extra_students(self):
        return self.extra_students

    def assigned(self):
        return self.assigned

    def identifyRowdyGroups(self, rowdy_groups):
        groups = []
        i = 0  # rowdy group index
        for rowdy_group in rowdy_groups:
            rowdy = False
            for student in rowdy_group:
                if student not in self.students:
                    rowdy = False
                    break
                else:
                    rowdy = True
            if rowdy == True:
                groups.append(i)
            i += 1

        for group in groups:
            self.rowdy_group_numbers.append(group)

    # add the number of the bus the community was assigned to
    def add_bus_number(self, bus_number):
        self.bus_numbers.append(bus_number)

    # this is when the community has been assigned. This doesn't mean there are no extra kids.
    def asssigned(self):
        self.assigned = True

    def breakRowdyGroups(self, rowdy_groups, graph):
        # take out one student from the rowdy group in the community and add that student to extra kids.
        # print("self.students: {}".format(self.students))
        # print("rowdy groups: {}".format(rowdy_groups))
        # print("rowdy group numbers: {}".format(self.rowdy_group_numbers))
        group = []
        removed = []
        for num in self.rowdy_group_numbers:
            # print("num: {}".format(num))
            # find the kid with the least number of friends and remove them from the rowdy group.
            g = rowdy_groups[num]
            group = [x for x in g if x not in removed]
            if group == []: continue
            # print("group: {}".format(group))
            values = [sum([1 for i in nx.all_neighbors(graph, student)]) for student in group]
            index = values.index(min(values))
            kid = group[index]
            # print("kid: {}".format(kid))
            removed.append(kid)
            self.extra_students.append(
                kid)  # add the kid with min number of friends to extra students.. to process later.
            self.students.remove(kid)
