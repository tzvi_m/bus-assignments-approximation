import networkx as nx
import random
import matplotlib.pyplot as plt
import math
import numpy as np
from networkx.algorithms import community
from networkx.algorithms.community.community_utils import is_partition
from networkx.algorithms.community import greedy_modularity_communities

from networkx.algorithms.community import quality
from networkx.algorithms.community import community_utils
from networkx.algorithms.community.community_utils import is_partition
from networkx.algorithms.community.quality import performance
from networkx.algorithms.community.quality import intra_community_edges
from networkx.algorithms.community.quality import inter_community_edges


# Makes inputs classes of kids with roudy groups and generates fake outputs
def generate_and_export(number_cliques, clique_size, reedge_probability, size):
    Z = nx.relaxed_caveman_graph(number_cliques, clique_size, reedge_probability, seed=42)
    z = list(Z.nodes)
    lenz = len(z)
    number_buses = lenz / 10
    bus_capcity = 10

    # export graph
    nx.write_gml(Z, "/Users/erinbeitel/projects/cs170/bus_assignments/inputs/{}/graph.gml".format(size))
    # graph stats:
    # average clique sizes, total number of kids, max clique size...
    # we want the bus capacity tightly above the clique size and clique size not to deviate much

    # export inputs
    random_roudy = []
    cliques = list(nx.find_cliques(Z))
    roudy = []
    for i in cliques:
        p = random.randint(1, 11)
        if p < 2:
            roudy.append(i)
        elif p < 3:
            roudy.append(i[1:])
        elif p < 4:
            roudy.append(i[2:])
            random_roudy.append(i[0])
        elif p < 5:
            roudy.append(i[3:])
            random_roudy.append(i[0])

    while len(random_roudy) > 4:
        roudy.append(random_roudy[0:5])
        random_roudy = random_roudy[5:]
    roudy.append(random_roudy)

    # write inputs
    f = open("/Users/erinbeitel/projects/cs170/bus_assignments/inputs/{}/parameters.txt".format(size), "w")

    f.write("{}\n".format(math.floor(number_buses)))
    f.write("{}\n".format(bus_capcity))

    unique_roudy = []
    for m in roudy:
        unique_roudy.append(list(np.unique(m)))

    finalroudy = [x for x in unique_roudy if x != [] and len(x) != 1]
    for k in finalroudy:
        f.write("{}\n".format(k))
    f.close()

    # fake fill buses for part 1:
    file = open("/Users/erinbeitel/projects/cs170/bus_assignments/outputs/{}.out".format(size), "w")
    nodes = z
    buses = []
    while nodes:
        file.write("{}\n".format(nodes[0:10]))
        buses.append(nodes[0:10])
        nodes = nodes[10:]
    file.close()

    return buses


# bus size 10
# generate graph gml files
# sm = generate_and_export(4, 10, .15, "small") # => size: 25-50 ~40
# print(sm)
# md = generate_and_export(40, 10, .15, "medium") # => size: 250-500 ~400
# print(md)
# lg = generate_and_export(80, 10, .15, "large") # => size: 500-1000 ~800
# print(lg)

def visualize(path, size, number):
    G = nx.read_gml(path + "/" + size + "/" + number + "/graph.gml")
    nx.draw(G, with_labels=True, font_weight='bold')
    plt.show()


# def assign_students(path):
# 	path_to_gml = path + "/graph.gml"
# 	path_to_params = path + "/parameters.txt"
# 	G = nx.read_gml(path_to_gml)
# 	greedy_modularity_communities = greedy_modularity_communities(G)
# 	parameters_file = open(path_to_params, “w”)
# 	params = parameters_file.readlines()
# 	number_of_busses = params.pop(0)
# 	number_roudy_groups = params.pop(0)
# 	roundy_groups = params
# 	parameters_file.close()
# 	communities = sorted(greedy_modularity_communities, key = lambda x: intra_community_edges(G, [x]))

# 	later_kids = {} # kid and bus they can't go in


# for each comminity:
# removed_kids = assign_community(communigty, graph) # say what bus number...

# add_removed_kids_to_later_kids(removed_kids, later_kids)

# ds for kid in bus_assignment
# ds for mapping communities (or really the roudy groups that originated in a given community) to buses.

# for each kid in later_kids:
# 	assign_kid(kid, graph, bus assignments)

# write bus_assignment to output file.


# def assign_kids(*):


# def assign_community()
# removed_kids, community = break_roundy_groups(*)
#   return removed_kids

# def break_roundy_groups(*):


# for our small input:
path = "/Users/erinbeitel/projects/cs170/bus_assignments/inputs/small"
# assign_students(path)

# for our medium input:
# path = "/Users/erinbeitel/projects/cs170/bus_assignments/inputs/medium"
# assign_students(path)

# # for small input:
# for i in range(1,331+1):
# 	path = "/Users/erinbeitel/projects/cs170/bus_assignments/all_inputs/small/{}".format(i)
# 	assign_students(path)
# end

# # for medium input:
# for j in range(1,331+1):
# 	path = "/Users/erinbeitel/projects/cs170/bus_assignments/all_inputs/medium/{}".format(j)
# 	assign_students(path)
# end


# visualize:
path = "/Users/erinbeitel/projects/cs170/bus_assignments/all_inputs"
size = "medium"
number = "67"
visualize(path, size, number)
