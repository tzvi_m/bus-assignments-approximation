import networkx as nx
import os
from output_scorer import score_output


def rowdy_solver(graph, num_buses, size_bus, constraints, size, submission_num):

    input_path = "/Users/tzvimashiach/Desktop/cs170/bus_assignments/inputs/{}/{}".format(size, submission_num)
    output_path = "/Users/tzvimashiach/Desktop/cs170/bus_assignments/rowdy_solver_outputs/{}/{}".format(size, submission_num)
    file = open(output_path, "w")
    kids = list(graph.nodes)
    grapher = kids.copy()
    # kids = graph
    # SET FOR ALL MINUS ONE ROWDY KIDS TO BE POPULATED RIGHT BEFORE NOW ROWDY KIDS ARE BEING PLACED IN BUSES
    set_of_min_kids = list()
    no_hope_list = list()
    temp_no_hope_list = list()

    # CREATING A MUTABLE VERSION OF CONSTRAINTS SO THAT I CAN REFER BACK TO A LIST
    # THAT HAS REMOVED ROWDY KIDS THAT HAVE BEEN PLACED ON BUSES
    mutable_constraints = list()
    for c in constraints:
        mutable_constraints.append(c.copy())


    # CREATES HASH OF BUSES WITH EACH VALUE AS AN ARRAY OF KIDS
    buses = dict()
    for j in range(num_buses):
        buses["bus{}".format(j + 1)] = list()  # CAN THROW IT IN A LIST WHEN NEEDED

    single_rowdy_groups(mutable_constraints, no_hope_list, temp_no_hope_list)

    while len(mutable_constraints) > 0:
        if len(mutable_constraints) != 0:

            # FINDS SMALLEST ROWDY GROUP
            smallest_rowdy_group = mutable_constraints[0]
            for n in mutable_constraints:
                if len(n) < len(smallest_rowdy_group):
                    smallest_rowdy_group = n
            deletable = smallest_rowdy_group

            # GIVES A DICTIONARY OF A KID FOR KEY AND 0 FOR VALUE
            rowdy_dictionary = {key: 0 for key in smallest_rowdy_group}

            # POPULATES THE DICTIONARY VALUES WITH THE AMOUNT OF TIMES THEY APPEAR IN ROWDY BUSES
            for i in smallest_rowdy_group:
                for n in mutable_constraints:
                    for m in n:
                        if i == m:
                            rowdy_dictionary[i] += 1
                            # MAYBE I CAN REMOVE THE ITEM AT THIS POINT
            mutable_constraints.remove(deletable)

            # REMOVES ALL KIDS FROM PREVIOUS ROWDY GROUPS CONSTRAINTS
            for i in smallest_rowdy_group:
                for n in mutable_constraints:
                    for m in n:
                        if i == m:
                            n.remove(i)
                            if len(n) == 0:
                                mutable_constraints.remove(n)

            # FIND MIN VALUE IN DICT, AND SEPARATE INTO ITS OWN LIST
            # ROWDY DICTIONARY GETS UPDATED TO REPRESENT THE REMOVED CHILD
            min_kid = min(rowdy_dictionary, key=rowdy_dictionary.get)
            if len(rowdy_dictionary) > 1:
                set_of_min_kids.append(min_kid)
                rowdy_dictionary.pop(min_kid)

            # smallest_rowdy_group has now been modified and can be added to a bus
            # now it might be good to use a hash to designate which buses the student should not be allowed in
            # FIND A BUS WITH (MIN SIZE && DOESNT CREATE A ROWDY GROUP) TO POPULATE
            temp_rowdy_list_minus_the_one = list()
            for key in rowdy_dictionary:
                temp_rowdy_list_minus_the_one.append(key)

            # HELPER FUNCTION CALL
            # NOW POPULATE BUSES WITH BOTH MIN_KID AND SMALLEST_ROWDY_DICTIONARY

            populate_buses(buses, constraints, temp_rowdy_list_minus_the_one, size_bus)

    # HELPER FUNCTION CALL
    populate_minus_one_kids(set_of_min_kids, buses, constraints, size_bus)

    # REMOVE ALL ROWDY KIDS FROM UNIVERSE OF KIDS

    spread_constraints = set()
    for c in constraints:
        for i in c:
            if i not in spread_constraints:
                spread_constraints.add(i)

    kids = set(kids)

    kids.difference_update(spread_constraints)

    kids = list(kids)
    new_kids = kids.copy()

    # ADD EVERY KID TO SOME BUS THAT HAS ROOM
    while len(new_kids) > 0:
        for k in new_kids:
            counter = 0
            while counter < len(buses):
                for key in buses:
                    if len(buses[key]) == 0:
                        if k in new_kids:
                            buses[key].append(k)
                            new_kids.remove(k)
                    else:
                        counter += 1
            for key in buses:
                if len(buses[key]) < size_bus:
                    if k in new_kids:
                        buses[key].append(k)
                        new_kids.remove(k)

    populate_single_rowdy_groups(no_hope_list, buses, size_bus)

    # TRANSFER HASH OF BUSES TO SUB-ARRAYS TO MAKE THEIR TRANSFER EASIER
    sub_array_filled_buses = list()
    for value in buses.values():
        sub_array_filled_buses.append(value)

    for g in grapher:
        counter = 0
        for i in sub_array_filled_buses:
            for j in i:
                if j == g:
                    counter += 1
                    if counter > 1:
                        i.remove(j)

    for b in sub_array_filled_buses:
        file.write("{}\n".format(b))

    file.close()
    score = score_output(input_path, output_path)  # score is returned as a tuple
    print(sub_array_filled_buses)
    return score[0], output_path


def populate_buses(buses, constraints, temp_rowdy_list_minus_the_one, size_bus):
    # POPULATE THE BUSES WITH MINUS ONE KIDS WITH THE CONDITION
    # THAT THEY CANNOT CREATE ANOTHER ROWDY GROUP

    while len(temp_rowdy_list_minus_the_one) > 0:
        for s in temp_rowdy_list_minus_the_one:  # for each minus ones kid
            while s in temp_rowdy_list_minus_the_one:
                counter = 0
                while counter < len(temp_rowdy_list_minus_the_one):
                    for key in buses:
                        if len(buses[key]) == 0:
                            if s in temp_rowdy_list_minus_the_one:
                                buses[key].append(s)
                                temp_rowdy_list_minus_the_one.remove(s)
                        else:
                            counter += 1

                for key in buses:  # we start out with the first array
                    key_flag = True
                    if s not in temp_rowdy_list_minus_the_one:
                        break
                    while s in temp_rowdy_list_minus_the_one and key_flag == True:
                        x = set(buses[key])
                        x.add(s)
                        for r in constraints:
                            if set(r).issubset(x) or len(x) > size_bus:
                                key_flag = False
                            else:
                                continue  # keep looking through rowdy groups
                        if key_flag:
                            buses[key].append(s)
                            temp_rowdy_list_minus_the_one.remove(s)


def populate_minus_one_kids(set_of_minus_one_kids, buses, some_rowdy_group, size_bus):
    # POPULATE THE BUSES WITH MINUS ONE KIDS WITH THE CONDITION
    # THAT THEY CANNOT CREATE ANOTHER ROWDY GROUP

    while len(set_of_minus_one_kids) > 0:
        for s in set_of_minus_one_kids:  # for each minus ones kid
            while s in set_of_minus_one_kids:
                counter = 0
                while counter < len(set_of_minus_one_kids):
                    for key in buses:
                        if len(buses[key]) == 0:
                            if s in set_of_minus_one_kids:
                                buses[key].append(s)
                                set_of_minus_one_kids.remove(s)
                        else:
                            counter += 1

                for key in buses:  # we start out with the first array
                    key_flag = True
                    if s not in set_of_minus_one_kids:
                        break
                    while s in set_of_minus_one_kids and key_flag == True:
                        x = set(buses[key])
                        x.add(s)
                        for r in some_rowdy_group:
                            if set(r).issubset(x) or len(x) > size_bus:
                                key_flag = False
                            else:
                                continue  # keep looking through rowdy groups
                        if key_flag:
                            buses[key].append(s)
                            set_of_minus_one_kids.remove(s)


def single_rowdy_groups(mutable_constraints, no_hope_list, temp_no_hope_list):

    for i in mutable_constraints:
        if len(i) == 1:
            temp_no_hope_list.append(i)

    for n in temp_no_hope_list:
        for m in n:
            if m not in no_hope_list:
                no_hope_list.append(m)

    for t in no_hope_list:
        for i in mutable_constraints:
            for j in i:
                if j == t:
                    i.remove(j)

    counter = 0
    while counter < len(mutable_constraints) + 1:
        counter += 1
        for i in mutable_constraints:
            if len(i) == 0:
                mutable_constraints.remove(i)


def populate_single_rowdy_groups(no_hope_list, buses, size_bus):

    while len(no_hope_list) > 0:
        for k in no_hope_list:
            for key in buses:
                if len(buses[key]) < size_bus:
                    if k in no_hope_list:
                        buses[key].append(k)
                        no_hope_list.remove(k)
