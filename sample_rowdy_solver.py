from random import randint, shuffle

rowdyDictionary = dict()
rowdy_set = list()
students = list()
mutable_constraints = list()
no_hope_list = list()
temp_no_hope_list = list()
constraints = rowdy_set
size = 0
num = 0

num_buses = 20
size_bus = 8
# buses * size of bus must be >= number of students


# SAMPLE INPUT GENERATOR TO TEST ROWDY_SOLVE FUNCTION
def input_generator(students_array, total_students=150, total_rowdy_sets=300, smallest_rowdy_group=2, largest_rowdy_group=5):
    i = 0
    while i < total_students:
        v = '{}'.format(i + 1)
        students_array.append(v)
        i += 1
    for _ in range(total_rowdy_sets):  # 20 random rowdy sets
        some_set = []
        shuffle(students_array)
        num_elements = randint(smallest_rowdy_group, largest_rowdy_group)
        for j in range(num_elements):  # the number of elements for the given rowdy set
            some_set.append(students_array[j])
        rowdy_set.append(some_set)


input_generator(students)
graph = students


# MAIN GREEDY APPROXIMATION FUNCTION
def rowdy_solve(graph, num_buses, size_bus, constraints, size, num):

    kids = graph
    # SET FOR ALL MINUS ONE ROWDY KIDS TO BE POPULATED RIGHT BEFORE NOW ROWDY KIDS ARE BEING PLACED IN BUSES
    set_of_min_kids = list()

    # CREATING A MUTABLE VERSION OF CONSTRAINTS SO THAT I CAN REFER BACK TO A LIST
    # THAT HAS REMOVED ROWDY KIDS THAT HAVE BEEN PLACED ON BUSES
    for c in constraints:
        mutable_constraints.append(c.copy())

    # SNUFFS OUT SINGLE ROWDY GROUPS AND PUTS THEM IN A SEPERATE LIST
    single_rowdy_groups(mutable_constraints, no_hope_list, temp_no_hope_list)

    # some_rowdy_group = [group for group in constraints]

    # CREATES HASH OF BUSES WITH EACH VALUE AS AN ARRAY OF KIDS
    buses = dict()
    for j in range(num_buses):
        buses["bus{}".format(j + 1)] = list()  # CAN THROW IT IN A LIST WHEN NEEDED

    while len(mutable_constraints) > 0:
        if len(mutable_constraints) != 0:

            # FINDS SMALLEST ROWDY GROUP
            smallest_rowdy_group = mutable_constraints[0]
            for n in mutable_constraints:
                if len(n) < len(smallest_rowdy_group):
                    smallest_rowdy_group = n
            deletable = smallest_rowdy_group
            # mutable_constraints.remove(deletable)

            # GIVES A DICTIONARY OF A KID FOR KEY AND 0 FOR VALUE
            rowdy_dictionary = {key: 0 for key in smallest_rowdy_group}

            # POPULATES THE DICTIONARY VALUES WITH THE AMOUNT OF TIMES THEY APPEAR IN ROWDY BUSES
            for i in smallest_rowdy_group:
                for n in mutable_constraints:
                    for m in n:
                        if i == m:
                            rowdy_dictionary[i] += 1
                            # MAYBE I CAN REMOVE THE ITEM AT THIS POINT
            mutable_constraints.remove(deletable)

            # REMOVES ALL KIDS FROM PREVIOUS ROWDY GROUPS CONSTRAINTS
            for i in smallest_rowdy_group:
                for n in mutable_constraints:
                    for m in n:
                        if i == m:
                            n.remove(i)
                            if len(n) == 0:
                                mutable_constraints.remove(n)

            # FIND MIN VALUE IN DICT, AND SEPARATE INTO ITS OWN LIST
            # ROWDY DICTIONARY GETS UPDATED TO REPRESENT THE REMOVED CHILD
            min_kid = min(rowdy_dictionary, key=rowdy_dictionary.get)
            if len(rowdy_dictionary) > 1:
                set_of_min_kids.append(min_kid)
                rowdy_dictionary.pop(min_kid)

            # smallest_rowdy_group has now been modified and can be added to a bus
            # now it might be good to use a hash to designate which buses the student should not be allowed in
            # FIND A BUS WITH (MIN SIZE && DOESNT CREATE A ROWDY GROUP) TO POPULATE
            temp_rowdy_list_minus_the_one = list()
            for key in rowdy_dictionary:
                temp_rowdy_list_minus_the_one.append(key)

            # HELPER FUNCTION CALL
            # NOW POPULATE BUSES WITH BOTH MIN_KID AND SMALLEST_ROWDY_DICTIONARY

            populate_buses(buses, constraints, temp_rowdy_list_minus_the_one, size_bus)

    # HELPER FUNCTION CALL
    populate_minus_one_kids(set_of_min_kids, buses, constraints, size_bus)

    # REMOVE ALL ROWDY KIDS FROM UNIVERSE OF KIDS
    spread_constraints = set()
    for c in constraints:
        for i in c:
            if i not in spread_constraints:
                spread_constraints.add(i)
    kids = set(kids)
    kids.difference_update(spread_constraints)
    kids = list(kids)
    new_kids = kids.copy()

    # ADD EVERY KID TO SOME BUS THAT HAS ROOM
    populate_non_rowdys(new_kids, buses)

    # POPULATE SINGLE ROWDY GROUPS
    populate_single_rowdy_groups(no_hope_list, buses, size_bus)

    # TRANSFER HASH OF BUSES TO SUB-ARRAYS TO MAKE THEIR TRANSFER EASIER
    sub_array_filled_buses = list()
    for value in buses.values():
        sub_array_filled_buses.append(value)

    # REMOVES ANY DUPLICATES --> DUPLICATES ARE RARE
    for g in graph:
        counter = 0
        for i in sub_array_filled_buses:
            for j in i:
                if j == g:
                    counter += 1
                    if counter > 1:
                        i.remove(j)

    return sub_array_filled_buses


def populate_buses(buses, constraints, temp_rowdy_list_minus_the_one, size_bus):

    # POPULATE THE BUSES WITH MINUS ONE KIDS WITH THE CONDITION
    # THAT THEY CANNOT CREATE ANOTHER ROWDY GROUP

    while len(temp_rowdy_list_minus_the_one) > 0:
        for s in temp_rowdy_list_minus_the_one:  # for each minus ones kid
            while s in temp_rowdy_list_minus_the_one:
                counter = 0
                while counter < len(temp_rowdy_list_minus_the_one):
                    for key in buses:
                        if len(buses[key]) == 0:
                            if s in temp_rowdy_list_minus_the_one:
                                buses[key].append(s)
                                temp_rowdy_list_minus_the_one.remove(s)
                        else:
                            counter += 1

                for key in buses:  # we start out with the first array
                    key_flag = True
                    if s not in temp_rowdy_list_minus_the_one:
                        break
                    while s in temp_rowdy_list_minus_the_one and key_flag == True:
                        x = set(buses[key])
                        x.add(s)
                        for r in constraints:
                            if set(r).issubset(x) or len(x) > size_bus:
                                key_flag = False
                            else:
                                continue  # keep looking through rowdy groups
                        if key_flag:
                            buses[key].append(s)
                            temp_rowdy_list_minus_the_one.remove(s)


def populate_minus_one_kids(set_of_minus_one_kids, buses, some_rowdy_group, size_bus):

    # POPULATE THE BUSES WITH MINUS ONE KIDS WITH THE CONDITION
    # THAT THEY CANNOT CREATE ANOTHER ROWDY GROUP

    while len(set_of_minus_one_kids) > 0:
        for s in set_of_minus_one_kids:  # for each minus ones kid
            while s in set_of_minus_one_kids:
                counter = 0
                while counter < len(set_of_minus_one_kids):
                    for key in buses:
                        if len(buses[key]) == 0:
                            if s in set_of_minus_one_kids:
                                buses[key].append(s)
                                set_of_minus_one_kids.remove(s)
                        else:
                            counter += 1

                for key in buses:  # we start out with the first array
                    key_flag = True
                    if s not in set_of_minus_one_kids:
                        break
                    while s in set_of_minus_one_kids and key_flag == True:
                        x = set(buses[key])
                        x.add(s)
                        for r in some_rowdy_group:
                            if set(r).issubset(x) or len(x) > size_bus:
                                key_flag = False
                            else:
                                continue  # keep looking through rowdy groups
                        if key_flag:
                            buses[key].append(s)
                            set_of_minus_one_kids.remove(s)


def populate_non_rowdys(new_kids, buses):

    while len(new_kids) > 0:
        for k in new_kids:
            counter = 0
            while counter < len(buses):
                for key in buses:
                    if len(buses[key]) == 0:
                        if k in new_kids:
                            buses[key].append(k)
                            new_kids.remove(k)
                    else:
                        counter += 1
            for key in buses:
                if len(buses[key]) < size_bus:
                    if k in new_kids:
                        buses[key].append(k)
                        new_kids.remove(k)


def single_rowdy_groups(mutable_constraints, no_hope_list, temp_no_hope_list):
    for i in mutable_constraints:
        if len(i) == 1:
            temp_no_hope_list.append(i)
            # mutable_constraints.remove(i)

    for n in temp_no_hope_list:
        for m in n:
            if m not in no_hope_list:
                no_hope_list.append(m)

    # print(no_hope_list)

    for t in no_hope_list:
        for i in mutable_constraints:
            for j in i:
                if j == t:
                    i.remove(j)

    counter = 0
    while counter < len(mutable_constraints) + 1:
        counter += 1
        for i in mutable_constraints:
            if len(i) == 0:
                mutable_constraints.remove(i)


def populate_single_rowdy_groups(no_hope_list, buses, size_bus):
    while len(no_hope_list) > 0:
        for k in no_hope_list:
            for key in buses:
                if len(buses[key]) < size_bus:
                    if k in no_hope_list:
                        buses[key].append(k)
                        no_hope_list.remove(k)


filled_buses = rowdy_solve(graph, num_buses, size_bus, constraints, size, num)


# AFTER MAIN FUNCTION HAS RUN, THE FOLLOWING LINES WILL TEST HOW IT FARED

# CHECKS FOR ROWDY GROUP IN POPULATED BUS
subset = False
for x in filled_buses:
    x = set(x)
    for c in constraints:
        if set(c).issubset(x):
            subset = True

count = 0
for i in filled_buses:
    for j in i:
        count += 1

print('\n')
print("Total in filled buses = ", count, '\n')
print("Total number of kids = ", len(graph), '\n')
duplicate = False
if count == len(graph):
    print("There are NO duplicates in filled buses.", '\n')
else:
    print("There are duplicates in filled buses.", '\n')
    duplicate = True

duplicate_list = list()
if duplicate is True:
    for g in graph:
        counter = 0
        for array in filled_buses:
            for a in array:
                if a == g:
                    counter += 1
                    if counter > 1:
                        duplicate_list.append(a)

if len(duplicate_list) > 0:
    print("Duplicate List = ", duplicate_list, '\n')


print("Kids = ", students, '\n')
print("Rowdy groups = ", constraints, '\n')
print("Filled buses = ", rowdy_solve(graph, num_buses, size_bus, constraints, size, num), '\n')
print("Subset of rowdy groups in filled buses = ", subset, '\n')
# checks to see if there is a subset of a rowdy group in any filled bus
