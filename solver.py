import networkx as nx
import os
from output_scorer import score_output  # added
from combination_solver import combination_solve
from rowdy_solver import rowdy_solver
import score_manager as score_manager
from random import shuffle

###########################################
# Change this variable to the path to
# the folder containing all three input
# size category folders
###########################################
path_to_inputs = "./all_inputs"

###########################################
# Change this variable if you want
# your outputs to be put in a
# different folder
###########################################
path_to_outputs = "./outputs"


def parse_input(folder_name):
    '''
        Parses an input and returns the corresponding graph and parameters

        Inputs:
            folder_name - a string representing the path to the input folder

        Outputs:
            (graph, num_buses, size_bus, constraints)
            graph - the graph as a NetworkX object
            num_buses - an integer representing the number of buses you can allocate to
            size_buses - an integer representing the number of students that can fit on a bus
            constraints - a list where each element is a list vertices which represents a single rowdy group
    '''
    graph = nx.read_gml(folder_name + "/graph.gml")
    parameters = open(folder_name + "/parameters.txt")
    num_buses = int(parameters.readline())
    size_bus = int(parameters.readline())
    constraints = []

    for line in parameters:
        line = line[1: -2]
        curr_constraint = [num.replace("'", "") for num in line.split(", ")]
        constraints.append(curr_constraint)

    return graph, num_buses, size_bus, constraints


# returns path to best assignment
def solve(graph, num_buses, size_bus, constraints, size, num):
    solution_path, score = assign_students(graph, num_buses, size_bus, constraints, size, num)
    return solution_path, score
    # pass


def assign_students(graph, num_buses, size_bus, constraints, size, num):
    solver_results = {}

    # random solver
    randomsover_score, random_path = random_solver(graph, num_buses, size_bus, constraints, size, num)
    solver_results[randomsover_score[0]] = random_path

    # combination solver
    combination_score, combination_path = combination_solve(graph, num_buses, size_bus, constraints, size, num)
    print(combination_score)
    solver_results[combination_score[0]] = combination_path

    # rowdy_score, rowdy_path = rowdy_solver(graph, num_buses, size_bus, constraints, size, num)
    # solver_results[rowdy_score[0]] = rowdy_path

    # add other solvers here

    best_score = max(solver_results.keys())
    print("{}: {} - SCORE: {}".format(size, num, best_score))
    best_output_path = solver_results[best_score]
    return best_output_path, best_score


def random_solver(graph, num_buses, size_bus, constraints, size, submission_num):
    input_path = "/Users/erinbeitel/projects/cs170/bus_assignments/all_inputs/{}/{}".format(size, submission_num)
    output_path = "/Users/erinbeitel/projects/cs170/bus_assignments/randomsolver_outputs/{}/{}.out".format(size,
                                                                                                           submission_num)
    file = open(output_path, "w")
    nodes = list(graph.nodes)
    shuffle(nodes)  # Shuffles the list so every time the random solver runs the result is slightly different.
    buses = []
    for j in range(0, num_buses):
        buses.append([])

    i = 0
    save = []
    for n in nodes:
        assignment = i % num_buses
        if is_bus_full(buses[assignment], size_bus) == False:
            buses[assignment].append(n)
        else:
            save.append(n)
        i += 1

    index = 0
    for s in save:
        while index < len(buses):
            if is_bus_full(buses[index], size_bus) == False:
                buses[index].append(s)
                break
            index += 1
        index = 0

    for b in buses:
        file.write("{}\n".format(b))

    if len(buses) > num_buses:
        print("Random solver used too many buses for {} input number {}".format(size, submission_num))
    file.close()
    score = score_output(input_path, output_path)  # score is returned as a tuple
    return score, output_path


def is_bus_full(bus, capacity):
    if len(bus) > capacity:
        print("bus was over capacity!")
    return len(bus) >= capacity


def run_single_input(size, input_name):
    path = "/Users/erinbeitel/projects/cs170/bus_assignments/all_inputs/{}/{}".format(size, input_name)
    graph, num_buses, size_bus, constraints = parse_input(path)
    solution, score = solve(graph, num_buses, size_bus, constraints, size, input_name)

    score_summary = {}
    current = max(score_manager.old_score(solution, score), score)
    if current < .3:
        current = str(current) + "  * VERY low *"
    elif current < .5:
        current = str(current) + "  * low *"
    else:
        current = str(current)

    score_summary[size + input_name] = current

    out_path = "/Users/erinbeitel/projects/cs170/bus_assignments/outputs/{}/{}".format(size, input_name)

    if score_manager.is_score_better(solution, score):
        output_file = open(out_path + ".out", "w")

        with open(solution) as f:
            lines = f.readlines()
        output_file.writelines(lines)
        score_manager.update_score(solution, score)

        f.close()

        output_file.close()


def main():
    '''
        Main method which iterates over all inputs and calls `solve` on each.
        The student should modify `solve` to return their solution and modify
        the portion which writes it to a file to make sure their output is
        formatted correctly.
    '''
    score_summary = {}
    size_categories = ["large"]  # ["small", "medium"]#, ["large"]
    if not os.path.isdir(path_to_outputs):
        os.mkdir(path_to_outputs)

    for size in size_categories:
        category_path = path_to_inputs + "/" + size
        output_category_path = path_to_outputs + "/" + size
        category_dir = os.fsencode(category_path)

        if not os.path.isdir(output_category_path):
            os.mkdir(output_category_path)

        for input_folder in os.listdir(category_dir):
            input_name = os.fsdecode(input_folder)
            if "." in input_name: continue  # added
            graph, num_buses, size_bus, constraints = parse_input(category_path + "/" + input_name)
            solution, score = solve(graph, num_buses, size_bus, constraints, size, input_name)  # added params

            # Only save the new output if it is better than the existing output.
            current = max(score_manager.old_score(solution, score), score)
            if current < .3:
                current = str(current) + "  * VERY low *"
            elif current < .5:
                current = str(current) + "  * low *"
            else:
                current = str(current)

            score_summary[size + input_name] = current

            if score_manager.is_score_better(solution, score):
                output_file = open(output_category_path + "/" + input_name + ".out", "w")

                with open(solution) as f:
                    lines = f.readlines()
                output_file.writelines(lines)
                score_manager.update_score(solution, score)

                f.close()
                output_file.close()

    for s in score_summary:
        print(s + " : " + score_summary[s])


if __name__ == '__main__':
    main()
